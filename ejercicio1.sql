create table provincias(
    PRO_ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    PRO_DESCRIPCION VARCHAR(45) NOT NULL,
    PRIMARY KEY(PRO_ID)
); 
 

create table partidos(
    PAR_ID int unsigned not null auto_increment,
    PRO_ID int unsigned not null,
    PAR_DESCRIPCION varchar(45) not null, 
    primary key (PAR_ID),
    CONSTRAINT FK_partidos_provincia FOREIGN KEY FK_partidos_provincia (PRO_ID)
        references provincias (PRO_ID)
        on delete restrict
        on update restrict
); 

insert into provincias(PRO_DESCRIPCION) values ('Formosa');
insert into provincias(PRO_DESCRIPCION) values ('Santa Cruz');
insert into provincias(PRO_DESCRIPCION) values ('Córdoba');
insert into provincias(PRO_DESCRIPCION) values ('Chubut');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Bermejo');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Matacos');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Pilagás');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Pilcomayo');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'El Calafate');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Las Heras');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Caleta Olivia');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Perito Moreno');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Calamuchita');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Cruz del Eje');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Minas');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Pocho');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Atlántico');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Biedma');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Cushamen');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Escalante');


describe partidos;
DESCRIBE provincias; 

select * from provincias 
select * from partidos

delete from provincias 
where PRO_ID=1

delete from provincias 
where PRO_ID=5

delete from partidos
where PRO_ID=4
delete from provincias
where PRO_ID=4

update provincias set PRO_DESCRIPCION='bsas' where PRO_ID=1

update provincias set PRO_DESCRIPCION='buenos mates' where PRO_ID=3

update partidos set PAR_DESCRIPCION='el mejor' where PAR_ID=1

update provincias set PRO_DESCRIPCION='cba' where PRO_ID=2

